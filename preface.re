= はじめに


== 謝辞
原文を書かれたエフラットさん、原文を書くことになったきっかけのゴールドラット博士にこのような物事を考える機会を与えていただけたことに感謝いたします。

また、気軽な気持ちでこの文書を翻訳したいなと思ったことに賛同していただけた皆様に感謝いたします。


== 著作権

This is the appendix to the original release edition of The Choice, and will appear in all
future editions. All materials copyright Eliyahu M. Goldratt and Efrat Goldratt‐Ashlag,
2009. This document can not be copied or distributed in any form without written
permission of the author/s.
